package com.example.meuprojeto

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    var nomeUsuario = ""
    var idadeUsuario = 0
    lateinit var textViewNome: EditText

    fun mostraTextView(){
        Toast.makeText(this,
            textViewNome.text.toString(), Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txtIdade = findViewById<EditText>(R.id.campoIdade)
        val btnEnviar = findViewById<Button>(R.id.btnEnviar)
        textViewNome = findViewById<EditText>(R.id.campoNome)


        btnEnviar.setOnClickListener {
            nomeUsuario = textViewNome.text.toString()
            idadeUsuario = txtIdade.text.toString().toInt()

            val intent = Intent(this, Activity2::class.java)
            intent.putExtra("NOME", nomeUsuario)
            intent.putExtra("IDADE", idadeUsuario)
            startActivity(intent)
        }

    }
}
