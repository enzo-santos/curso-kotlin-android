package com.example.meuprojeto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class Activity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_activity_2)

        val txtView = findViewById<TextView>(R.id.text)
        val nome = intent.getStringExtra("NOME") ?: ""
        val idade = intent.getIntExtra("IDADE", 0)
        txtView.text = "Olá $nome, sua idade é $idade"
    }
}